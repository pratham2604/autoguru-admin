const functions = require('firebase-functions');
const admin = require('firebase-admin');
const db = admin.firestore();
const cors = require('cors');

exports = module.exports = functions.https.onRequest((request, response) => {
  db.collection('partners').onSnapshot((snapshot) => {
    const { docs } = snapshot;
    const data = docs.map(doc => doc.data());
    const res = {
      success: true,
      data,
    }
    response.set('Access-Control-Allow-Origin', "*");
    response.set('Access-Control-Allow-Credentials', true);
    response.set('Access-Control-Allow-Headers', 'content-type');
    response.send(res);
    return;
  });
});