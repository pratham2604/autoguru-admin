import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { fetchAuth, signOut } from '../actions/auth';
import { fetchAdmin, addAdmin, updateAdmin, fetchRoles, updateRoles, addRoles } from '../actions/admins';
import { fetchEvents, addEvents, updateEvents } from '../actions/events';
import { fetchVendors, addVendor, updateVendor, fetchVendorsCategories, updateVendorsCategories, addVendorCategories } from '../actions/vendors';
import { fetchUsers, updateUser } from '../actions/users';
import { fetchPartners, addPartners, updatePartners, deletePartners } from '../actions/partners';
import SideMenu from '../components/sideMenu';

const editMethodMap = {
  vendor: updateVendor,
  admin: updateAdmin,
  event: updateEvents,
  user: updateUser,
  category: updateVendorsCategories,
  role: updateRoles,
  partner: updatePartners,
};

const addMethodMap = {
  vendor: addVendor,
  admin: addAdmin,
  event: addEvents,
  category: addVendorCategories,
  role: addRoles,
  partner: addPartners,
}

const deleteMethodMap = {
  partner: deletePartners,
}

class Home extends Component {
  state = {
    loadComponent: false,
  }

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(fetchAuth(this.onSuccess, this.onFailure));
    dispatch(fetchAdmin());
    dispatch(fetchUsers());
    dispatch(fetchVendors());
    dispatch(fetchEvents());
    dispatch(fetchVendorsCategories());
    dispatch(fetchRoles());
    dispatch(fetchPartners());
  }

  onSuccess = (data) => {
    this.setState({
      loadComponent: true,
    });
  }

  onFailure = () => {
    const { history } = this.props;
    history.push('/login');
  }

  signOut = () => {
    const { dispatch } = this.props;
    dispatch(signOut(this.onSingOutSuccess, this.onSignOutFailure));
  }

  onSingOutSuccess = () => {
    const { history } = this.props;
    history.push('/login');
  }

  onSignOutFailure = (error) => {
    console.log(error);
  }

  addData = (data, type) => {
    const { dispatch } = this.props;
    dispatch(addMethodMap[type](data));
  }
  
  editData = (data, type) => {
    const { dispatch } = this.props;
    dispatch(editMethodMap[type](data));
  }

  deleteData = (data, type) => {
    const { dispatch } = this.props;
    dispatch(deleteMethodMap[type](data));
  }

  render() {
    const { loadComponent } = this.state;
    const { Layout, location, ...rest } = this.props;
    const { auth, admins, roles } = this.props;
    if (!loadComponent) {
      return null;
    };

    const currentAdmin = admins.find(admin => admin.email === auth.email);
    const currentUser = Object.assign({}, auth, currentAdmin);
    const currentUserRole = (roles || []).find(role => role.id === currentUser.role);
    const routesAccess = (currentUserRole || {}).access || [];

    return (
      <SideMenu auth={auth} admins={admins} roles={roles} location={location} signOut={this.signOut}>
        <Layout
          loadData={this.loadData}
          editData={this.editData}
          addData={this.addData}
          deleteData={this.deleteData}
          routesAccess={routesAccess}
          {...rest}
        />
      </SideMenu>
    )
  }
}

const mapStateToProps = state => ({
  users: state.users.data,
  events: state.events.data,
  admins: state.admins.data,
  vendors: state.vendors.data,
  auth: state.auth.user,
  categories: state.vendors.categories,
  roles: state.admins.roles,
  partners: state.partners.data,
});

const mapDispatchToProps = dispatch => ({
  dispatch,
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Home));