import { TEMPLATE_TYPES } from '../template/index';
import AuthContainer from '../../containers/auth';
import LoginComponent from '../../modules/auth/UI/login';

const loginRoute = {
  Container: AuthContainer,
  Component: LoginComponent,
  path: '/login',
  title: 'Login',
  templateType: TEMPLATE_TYPES.PURE,
}

export default [
  loginRoute,
];