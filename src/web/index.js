import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import { PersistGate } from 'redux-persist/lib/integration/react';
import configureStore from './store/index';
import * as serviceWorker from './serviceWorker/index';
import Routes from './routes/index';
import "bootstrap-css-only/css/bootstrap.min.css";
import 'semantic-ui-css/semantic.min.css';
import '../styles/styles.scss';

import '@fortawesome/fontawesome-free/css/all.min.css';
import "mdbreact/dist/css/mdb.css";

import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import * as Firebase from '../lib/firebase';

toast.configure({
  position: "top-right",
  autoClose: 5000,
  hideProgressBar: true,
  closeOnClick: true,
  pauseOnHover: true,
  draggable: true,
})

Firebase.LoadFirebase();

const { persistor, store } = configureStore();

const Root = () => (
  <Provider store={store}>
    <PersistGate persistor={persistor}>
      <Router>
        <Routes/>
      </Router>
    </PersistGate>
  </Provider>
)

ReactDOM.render(<Root />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
