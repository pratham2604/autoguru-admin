import Store from '../store/auth';
import * as TYPES from '../constants/types';

export const initialState = Store;

export default function authReducer(state = initialState, action) {
  switch (action.type) {
    case TYPES.FETCH_AUTH_REQUEST:
      return Object.assign({}, state, {
        fetching: true,
        isAuthenticated: false,
      });
    case TYPES.FETCH_AUTH_SUCCESS:
      return Object.assign({}, state, {
        user: action.data,
        fetching: false,
        token: action.token,
        error: null,
        isAuthenticated: true,
      });
    case TYPES.FETCH_AUTH_FAILURE:
      return Object.assign({}, state, {
        error: action.message,
        fetching: false,
        isAuthenticated: false,
        user: {},
        token: null,
      });
    default:
      return state;
  }
}