import auth from './auth';
import admins from './admins';
import events from './events';
import users from './users';
import vendors from './vendors';
import partners from './partners';

const rehydrated = (state = false, action) => {
  switch (action.type) {
    case 'persist/REHYDRATE':
      return true;
    default:
      return state;
  }
};

export default {
  rehydrated,
  auth,
  admins,
  events,
  users,
  vendors,
  partners,
};
