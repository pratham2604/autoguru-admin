import Store from '../store/admins';
import * as TYPES from '../constants/types';

export const initialState = Store;

export default function adminReducer(state = initialState, action) {
  switch (action.type) {
    case TYPES.FETCH_ADMINS_REQUEST:
      return Object.assign({}, state, {
        fetching: true,
        error: null,
        data: [],
      });
    case TYPES.FETCH_ADMINS_SUCCESS:
      return Object.assign({}, state, {
        data: action.data,
        fetching: false,
        error: null,
      });
    case TYPES.FETCH_ADMINS_FAILURE:
      return Object.assign({}, state, {
        error: action.message,
        fetching: false,
        data: [],
      });

    case TYPES.FETCH_ADMIN_ROLES_SUCCESS:
      return Object.assign({}, state, {
        roles: action.data,
        fetching: false,
        error: null,
      });
    default:
      return state;
  }
}