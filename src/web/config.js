import { TEMPLATE_TYPES } from './template/index';

const firebaseConfig = {
  apiKey: "AIzaSyDwy42gmgzZfzoG2CRXbmGb01k2YDWloRQ",
  authDomain: "autoguru-admin.firebaseapp.com",
  databaseURL: "https://autoguru-admin.firebaseio.com",
  projectId: "autoguru-admin",
  storageBucket: "autoguru-admin.appspot.com",
  messagingSenderId: "984539312116",
  appId: "1:984539312116:web:c4551b0a192371b63d13a1",
  measurementId: "G-V7L5C81LPF"
};

export default {
  DEFAULT_TEMPLATE: TEMPLATE_TYPES.NAVBAR,
  FIREBASE_CONFIG: firebaseConfig,
};