import PureTemplate from './pureTemplate';
import NavbarTemplate from './navbarTemplate';
import SidebarTemplate from './sidebarTemplate';
import HomeTemplate from './homeTemplate';

export const TEMPLATE = {
  PURE: PureTemplate,
  NAVBAR: NavbarTemplate,
  SIDEBAR: SidebarTemplate,
  HOME: HomeTemplate,
};

export const TEMPLATE_TYPES = {
  PURE: 'PURE',
  NAVBAR: 'NAVBAR',
  SIDEBAR: 'SIDEBAR',
  HOME: 'HOME',
};