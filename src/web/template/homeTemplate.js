import React, { Fragment, Component } from 'react';
import { Helmet } from 'react-helmet';
import { Container, Menu } from 'semantic-ui-react'

export default class extends Component {
  render() {
    const { children, pageTitle } = this.props;
    return (
      <Fragment>
        <Helmet>
          <title>{pageTitle}</title>
        </Helmet>
        <Menu fixed='top' inverted>
          <Container>
            <Menu.Item as='a' header>
              Project Name
            </Menu.Item>
            <Menu.Item as='a'>Home</Menu.Item>
          </Container>
        </Menu>
        <Container fluid style={{ marginTop: '7em' }}>
          {children}
        </Container>
      </Fragment>
    );
  }
}