import { Firestore, CreateFirebaseApp } from '../lib/firebase';
import * as TYPES from '../web/constants/types';
import { v4 as uuidv4 } from 'uuid';
import { toast } from 'react-toastify';
const FirebaseApp = CreateFirebaseApp();

export const addAdmin = (data, onFailure) => {
  const { email, password } = data;
  return (dispatch) => {
    FirebaseApp.auth().createUserWithEmailAndPassword(email, password).then(response => {
      const { user } = response;
      if (user) {
        const { email, emailVerified, uid, displayName, photoURL } = user;
        const userResponse = Object.assign({}, data, { email, emailVerified, uid, displayName, photoURL });
        Firestore.collection("admins").doc(uid).set(userResponse)
        .then((docRef) => {
          FirebaseApp.auth().signOut();
          toast.success('Admin added successfully');
        }).catch((error) => {
          toast.error(error);
          console.error("Error adding document: ", error);
          FirebaseApp.auth().signOut();
        });
      }
    }).catch(err => {
      const { message = 'Unable to add admin' } = err;
      toast.error(message);
    })
  }
}

export const fetchAdmin = () => {
  return (dispatch) => {
    dispatch({type: TYPES.FETCH_ADMINS_REQUEST});
    Firestore.collection("admins").onSnapshot((snapshot) => {
      const { docs } = snapshot;
      const data = docs.map(doc => doc.data());
      dispatch({type: TYPES.FETCH_ADMINS_SUCCESS, data});
    });
  }
}

export const updateAdmin = (data) => {
  const { uid } = data;
  return (dispatch) => {
    Firestore.collection("admins").doc(uid).update(data)
    .then((snapshot) => {
      toast.success('Admin updated successfully');
      console.log("Doc updated")
    }).catch((error) => {
      toast.error(error);
      console.error("Error loading documents: ", error);
    });
  }
}

export const fetchRoles = () => {
  return (dispatch) => {
    Firestore.collection("admin_roles").onSnapshot((snapshot) => {
      const { docs } = snapshot;
      const data = docs.map(doc => doc.data());
      dispatch({type: TYPES.FETCH_ADMIN_ROLES_SUCCESS, data});
    });
  }
}

export const addRoles =  (data) => {
  return (dispatch) => {
    data.id = uuidv4();
    Firestore.collection("admin_roles").doc(data.id).set(data)
    .then((docRef) => {
      toast.success('Role added successfully');
      console.log("Document written with ID: ");
    }).catch((error) => {
      toast.error(error);
      console.error("Error adding document: ", error);
    });
  }
}

export const updateRoles = (data) => {
  const { id } = data;
  return (dispatch) => {
    Firestore.collection("admin_roles").doc(id).update(data)
    .then((snapshot) => {
      toast.success('Role updated successfully');
      console.log("Doc updated")
    }).catch((error) => {
      toast.error(error);
      console.error("Error loading documents: ", error);
    });
  }
}