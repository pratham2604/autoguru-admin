import { Firestore } from '../lib/firebase';
import * as TYPES from '../web/constants/types';
import { v4 as uuidv4 } from 'uuid';
import { toast } from 'react-toastify';

export const fetchPartners = () => {
  return (dispatch) => {
    dispatch({type: TYPES.FETCH_PARTNERS_REQUEST});
    Firestore.collection("partners").onSnapshot((snapshot) => {
      const { docs } = snapshot;
      const data = docs.map(doc => doc.data());
      dispatch({type: TYPES.FETCH_PARTNERS_SUCCESS, data});
    });
  }
}

export const addPartners = (data) => {
  return (dispatch) => {
    data.id = uuidv4();
    Firestore.collection("partners").doc(data.id).set(data)
    .then((docRef) => {
      toast.success('Partner added successfully');
      console.log("Document written");
    }).catch((error) => {
      toast.error(error);
      console.error("Error adding document: ", error);
    });
  }
}

export const updatePartners = (data) => {
  const { id } = data;
  return (dispatch) => {
    Firestore.collection("partners").doc(id).update(data)
    .then((snapshot) => {
      toast.success('Partner updated successfully');
      console.log("Doc updated")
    }).catch((error) => {
      toast.error(error);
      console.error("Error loading documents: ", error);
    });
  }
}

export const deletePartners = (data) => {
  const { id } = data;
  return (dispatch) => {
    Firestore.collection("partners").doc(id).delete()
    .then((snapshot) => {
      toast.success('Partner deleted successfully');
      console.log("Doc deleted")
    }).catch((error) => {
      toast.error(error);
      console.error("Error loading documents: ", error);
    });
  }
}