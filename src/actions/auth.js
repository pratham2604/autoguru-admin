import * as firebase from "firebase/app";
import * as TYPES from '../web/constants/types';

export const fetchAuth = (onSuccess, onFailure) => {
  return (dispatch) => {
    dispatch({type: TYPES.FETCH_AUTH_REQUEST});
    return firebase.auth().onAuthStateChanged(user => {
      if (user) {
        const { email, emailVerified, uid, refreshToken, displayName, photoURL } = user;
        const data = Object.assign({}, { email, emailVerified, uid, displayName, photoURL });
        dispatch({
          type: TYPES.FETCH_AUTH_SUCCESS,
          data,
          token: refreshToken,
        });
        onSuccess(data);
      } else {
        const message = 'User auth not present';
        dispatch({type: TYPES.FETCH_AUTH_FAILURE, message});
        onFailure();
      }
    });
  }
}

export const login = (data, onSuccess, onFailure) => {
  const { email, password } = data;
  return (dispatch) => {
    firebase.auth().signInWithEmailAndPassword(email, password).then(response => {
      const { user } = response;
      if (user) {
        const { email, emailVerified, uid, refreshToken, displayName, photoURL } = user;
        const data = Object.assign({}, { email, emailVerified, uid, displayName, photoURL });
        dispatch({
          type: TYPES.FETCH_AUTH_SUCCESS,
          data,
          token: refreshToken,
        });
      }
      onSuccess(data);
    }).catch(err => {
      const { message = 'Unable to login' } = err;
      onFailure(message);
    })
  }
}

export const signOut = (onSuccess, onFailure) => {
  return (dispatch) => {
    return firebase.auth().signOut().then(res => {
      onSuccess();
    }).catch(err => {
      onFailure(err);
    });
  }
}