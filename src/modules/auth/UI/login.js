import React, { Component } from 'react'
import { Button, Form, Grid, Header, Segment, Message } from 'semantic-ui-react';

class LoginForm extends Component {
  state = {
    email: '',
    password: '',
  }

  onChange = (e, { name, value }) => {
    this.setState({
      [name]: value
    });
  }

  handleSubmit = () => {
    const { email, password } = this.state;
    const data = Object.assign({}, { email, password });
    this.props.login(data);
  }

  render() {
    const { password, email } = this.state;
    const { loginError } = this.props;

    return (
      <Grid textAlign='center' style={{ height: '100vh' }} verticalAlign='middle'>
        <Grid.Column style={{ maxWidth: 450 }}>
          <Header as='h2' textAlign='center'>
            Log-in to your account
          </Header>
          <Form size='large' onSubmit={this.handleSubmit}>
            <Segment stacked>
              <Form.Input fluid icon='user' name="email" iconPosition='left' placeholder='E-mail address' value={email} onChange={this.onChange}/>
              <Form.Input
                fluid
                icon='lock'
                name="password"
                iconPosition='left'
                placeholder='Password'
                type='password'
                value={password}
                onChange={this.onChange}
              />
              <Button type="submit" color='teal' fluid size='large'>
                Login
              </Button>
            </Segment>
            {loginError &&
              <Message negative>
                <p>{loginError}</p>
              </Message>
            }
          </Form>
        </Grid.Column>
      </Grid>
    );
  }
}

export default LoginForm
