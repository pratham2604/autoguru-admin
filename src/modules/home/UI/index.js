import React, { Component } from 'react';
import { Segment, Grid, Header, Button } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

const routes = [{
  title: 'Home',
  to: '/',
  show: true,
  id: 'home'
}, {
  title: 'Admins',
  to: '/admins',
  show: true,
  id: 'admin',
}
// , {
//   title: 'Users',
//   to: '/users',
//   show: true,
//   id: 'user',
// }
, {
  title: 'Partners',
  to: '/partners',
  show: true,
  id: 'partner',
}]

export default class extends Component {
  render() {
    const { admins, auth, roles } = this.props;
    // const activeUsersCount = users.filter(user => !user.isDisabled).length;
    // const activeVendorsCount = vendors.filter(vendor => !vendor.isDisabled).length;
    // const activeEvents = events.filter(event => !event.isDisabled);
    // const activeEventsCount = activeEvents.length;

    const currentAdmin = admins.find(admin => admin.email === auth.email);
    const currentUser = Object.assign({}, auth, currentAdmin);
    const currentUserRole = (roles || []).find(role => role.id === currentUser.role);
    const access = (currentUserRole || {}).access || [];

    return (
      <Segment>
        {/* <Grid columns={3} stackable>
          <Grid.Column>
            <Segment>
              <Header as='h3'>Active User</Header>
              <Header as='h1'>{activeUsersCount || 0}</Header>
            </Segment>
          </Grid.Column>
        </Grid> */}
        <Grid columns={2} stackable>
          <Grid.Column>
            <Segment>
              <Header as="h3">
                Check data here
              </Header>
              {routes.filter(route => {
                const routeAccess = access.find(accessData => accessData.id === route.id) || {};
                return routeAccess.view || routeAccess.edit || route.id === 'home';
              }).map(route => {
                const { to, title } = route;
                return (
                  <Button as={Link} to={to} key={title} style={{margin: '1em'}}>
                    {title}
                  </Button>
                )
              })}
            </Segment>
          </Grid.Column>
        </Grid>
      </Segment>
    )
  }
}