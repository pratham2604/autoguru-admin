import React, { Component } from 'react';
import { Button, Modal, Form, Input, Segment, Rating, TextArea, Label, Checkbox, Image, Header } from 'semantic-ui-react';
import { Map as LeafletMap, TileLayer, Marker, Popup } from 'react-leaflet';
import Swiper from 'react-id-swiper';
import moment from 'moment';

export default class extends Component {
  constructor(props) {
    super(props);
    const { data } = props;
    this.state = {
      approved: data.approved || false,
      capacity: data.capacity || 0,
      description: data.description || '',
      hashtags: data.hashtags || [],
      name: data.name || '',
    };
  }

  onChange = (e, { name, value }) => {
    this.setState({
      [name]: value
    });
  }

  onChecked = (e, { name, value}) => {
    this.setState({
      [name]: !this.state[name],
    })
  }

  onHashtagChange = (e, { name, value }) => {
    const tags = value.split(',');
    this.setState({
      hashtags: tags,
    });
  }

  handleSubmit = () => {
    const { data } = this.props;
    const { hashtags, approved, capacity, description, name } = this.state;
    const trimmedHashTags = hashtags.map(tag => tag.trim());
    const submitData = Object.assign({}, data, { name, description ,capacity, approved}, {
      hashtags: trimmedHashTags
    });
    this.props.editData(submitData, 'event');
  }

  render() {
    const { data, canEdit, users } = this.props;
    const { host, images, location, attendeeCount, attendees, performanceAt, rating, ratingCount = 0 } = data;
    const { approved, capacity, description, hashtags, name } = this.state;
    let hashtagsValue = '';
    (hashtags || []).forEach(tag => {
      hashtagsValue += tag + ','
    });
    if (hashtagsValue.length) {
      hashtagsValue = hashtagsValue.slice(0, -1);
    }

    return (
      <Modal trigger={<Button icon='edit' color='blue'/>} centered={false} closeIcon>
        <Modal.Header>Edit Event</Modal.Header>
        <Modal.Content>
          <Form onSubmit={this.handleSubmit}>
            <Form.Field>
              <Rating icon='star' defaultRating={rating} maxRating={5} />
              <Label>{`Total Reviews: ${ratingCount}`}</Label>
            </Form.Field>
            <Form.Field>
              <Checkbox content="Approved" toggle name="approved" checked={approved} onChange={this.onChecked}/>
            </Form.Field>
            <Form.Field>
              <label>Name</label>
              <Input placeholder='Name' name="name" value={name} onChange={this.onChange}/>
            </Form.Field>
            <Form.Field>
              <label>Description</label>
              <TextArea placeholder='Description' name="description" value={description} onChange={this.onChange}/>
            </Form.Field>
            <Form.Field>
              <label>Capacity</label>
              <Input type="number" placeholder='Capacity' name="capacity" value={capacity} onChange={this.onChange}/>
            </Form.Field>
            <Form.Field>
              <label>HashTags <span style={{fontWeight: 'normal'}}>(Add New hashtags separated by comma)</span></label>
              <Input placeholder='Hashtags' name="hashtags" value={hashtagsValue} onChange={this.onHashtagChange}/>
            </Form.Field>
            <LocationMap location={location} />
            <Gallery images={images} />
            <Attendees users={users} attendees={attendees} attendeeCount={attendeeCount} />
            <Segment>
              <Header as="h3">Host</Header>
              <div>
                <b>
                  {host.name}
                </b>
              </div>
            </Segment>
            <Segment>
              <Header as="h3">Performance At</Header>
              <div>
                <b>
                  {moment(performanceAt).format('DD-MMM-YY hh:mm a')}
                </b>
              </div>
            </Segment>
            {canEdit && <Button type='submit'>Submit</Button>}
          </Form>
        </Modal.Content>
      </Modal>
    )
  }
}

class Attendees extends Component {
  render() {
    const { users, attendeeCount = 0, attendees } = this.props;

    return (
      <Segment>
        <Header as="h3">Attendees</Header>
        <div style={{marginBottom: '1em'}}>
          <b>Attendee Count: {attendeeCount || 0}</b>
        </div>
        {attendees.map((attendee, index) => {
          const user = users.find(user => user.id === attendee);
          const { firstName = '', profilePic = '' } = user || {};
          return (
            <Label as='a' image key={index}>
              <img src={profilePic} alt="profile"/>
              {firstName}
            </Label>
          )
        })}
      </Segment>
    )
  }
}

class Gallery extends Component {
  render() {
    const params = {
      slidesPerView: 'auto',
      spaceBetween: 30,
      // loop: true,
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      }
    }
    const { images } = this.props;

    return (
      <Segment>
        <Swiper {...params}>
          {images.map((image, index) => {
            return (
              <Image style={{display: 'inline-block'}} src={image} size='small' key={index} alt="event"/>
            )
          })}
        </Swiper>
      </Segment>
    )
  }
}

class LocationMap extends Component {
  render() {
    const { location } = this.props;
    const events = [{
      id: '1',
      location,
    }];
    return (
      <Segment>
        <div className="map-container">
          <LeafletMap
            center={[50, 10]}
            zoom={1}
            maxZoom={10}
            attributionControl={true}
            zoomControl={true}
            doubleClickZoom={true}
            scrollWheelZoom={false}
            dragging={true}
            animate={true}
            easeLinearity={0.35}
          >
            <TileLayer
              attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
              url='https://{s}.tile.osm.org/{z}/{x}/{y}.png'
            />
            {events.map(event => {
                const { location, id } = event;
                const { latitude, longitude, city, fullAddress} = location;
                return (
                  <Marker position={[latitude, longitude]} key={id}>
                      <Popup>
                          <div>Address: {fullAddress}</div>
                          <div>City: {city}</div>
                          <div>Latitude: {latitude}</div>
                          <div>Longitude: {longitude}</div>
                      </Popup>
                  </Marker>
                );
            })}
          </LeafletMap>
        </div>
      </Segment>
    )
  }
}