import React, { Component } from 'react';
import { Button, Icon, Segment } from 'semantic-ui-react';
import { Form, Input, Header, List } from 'semantic-ui-react';

export default class Timeline extends Component {
  state = {
    editMode: false,
    year: '',
    title: '',
    events: [],
    editIndex: -1,
  }

  onChange = (e, { name, value }) => {
    this.setState({
      [name]: value,
    })
  }

  updateTimeline = (e) => {
    const { editIndex, title, year, events } = this.state;
    const { timeline = [], onChange } = this.props;
    if (editIndex === -1) {
      timeline.push({
        title,
        year,
        events,
      });
    } else {
      timeline[editIndex] = Object.assign({
        title,
        year,
        events,
      });
    }
    onChange(e, {name: 'timeline', value: timeline});
    this.setState({
      editMode: false,
      editIndex: -1,
      title: '',
      year: '',
      events: [],
    });
  }

  onDelete = (index) => {
    const { timeline = [], onChange } = this.props;
    timeline.splice(index, 1);
    onChange({}, {name: 'timeline', value: timeline});
  }

  onUpdate = (index) => {
    const { timeline = [] } = this.props;
    const { title, year, events } = timeline[index];
    this.setState({
      title,
      year,
      events,
      editIndex: index,
      editMode: true,
    })
  }

  render() {
    const { timeline = [] } = this.props;
    const { editMode, year, title, events } = this.state;
    return (
      <Form.Field>
        <label>Timeline</label>
        {timeline.map((info, index) =>
          <MileStone info={info} index={index} key={index} onDelete={this.onDelete} onUpdate={this.onUpdate} />
        )}
        <br />
        {editMode ?
          <Segment>
            <Input placeholder='Title' name="title" value={title} onChange={this.onChange} style={{marginBottom: '5px'}}/>
            <Input placeholder='Year' name="year" value={year} onChange={this.onChange} style={{marginBottom: '5px'}}/>
            <Events onChange={this.onChange} events={events} />
            <br /><br />
            <div>
              <Button type="button" positive disabled={!title || !events.length} onClick={this.updateTimeline}>Save to timeline</Button>
              <Button type="button" negative onClick={() => {this.setState({editMode: false, title: '', year: '', events: []})}}>Cancel</Button>
            </div>
          </Segment> :
          <Button type="button" positive onClick={() => {this.setState({editMode: true, editIndex: -1})}}>Add</Button>
        }
      </Form.Field>
    )
  }
}

class Events extends Component {
  state = {
    editMode: false,
    newEvent: '',
  }

  onChange = (e, { name, value }) => {
    this.setState({
      newEvent: value,
    });
  }

  addEvent = (e) => {
    const { events = [], onChange } = this.props;
    const { newEvent } = this.state;
    events.push(newEvent);
    onChange(e, {name: 'events', value: events});
    this.setState({
      editMode: false,
      newEvent: '',
    });
  }

  onDelete = (index) => {
    const { events = [], onChange } = this.props;
    events.splice(index, 1);
    onChange({}, {name: 'events', value: events});
  }

  onUpdate = (index, value) => {
    const { events = [], onChange } = this.props;
    events[index] = value;
    onChange({}, {name: 'events', value: events});
  }

  render() {
    const { events = [] } = this.props;
    const { editMode, newEvent } = this.state;
    return (
      <Form.Field>
        {events.map((event, index) => {
          return (
            <div style={{display: 'flex', marginBottom: '5px'}} key={index}>
              <Input placeholder='Event' value={event} onChange={(e) => {this.onUpdate(index, e.target.value)}} style={{marginRight: '1em'}}/>
              <Button icon onClick={this.onDelete.bind(this, index)} type="button">
                <Icon name="trash" />
              </Button>
            </div>
          )
        })}
        {editMode ?
          <div>
            <Input placeholder='Event' name="newEvent" value={newEvent} onChange={this.onChange}/>
            <br /><br />
            <div>
              <Button type="button" positive disabled={!newEvent} onClick={this.addEvent}>Add event</Button>
              <Button type="button" negative onClick={() => {this.setState({editMode: false, newEvent: ''})}}>Cancel</Button>
            </div>
          </div> :
          <Button type="button" positive onClick={() => {this.setState({editMode: true})}}>Add new event to year</Button>
        }
      </Form.Field>
    )
  }
}

class MileStone extends Component {
  render() {
    const { info, onUpdate, index, onDelete } = this.props;
    const { title = '', year, events = [] } = info || {};
    return (
      <div className="detailed-milestone-container">
        <div className="content-container">
          <div className="content">
            <Header as="h3" className="title" inverted>{title}</Header>
            <div className="year">{year}</div>
          </div>
          <List bulleted>
            {events.map((event, index) => <List.Item key={index} className="event">{event}</List.Item>)}
          </List>
        </div>
        <div>
          <Button icon onClick={onUpdate.bind(this, index)} type="button" style={{margin: '0 1em 1em 0'}}>
            <Icon name="pencil" />
          </Button>
          <Button icon onClick={onDelete.bind(this, index)} type="button">
            <Icon name="trash" />
          </Button>
        </div>
      </div>
    )
  }
}
