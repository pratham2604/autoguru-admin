import React, { Component } from 'react';
import { Button, Icon } from 'semantic-ui-react';
import { Form, Input, TextArea } from 'semantic-ui-react';

export default class FAQs extends Component {
  state = {
    editMode: false,
    question: '',
    answer: '',
    editIndex: -1,
  }

  onChange = (e, { name, value }) => {
    this.setState({
      [name]: value,
    })
  }

  updateFAQ = (e) => {
    const { editIndex, question, answer } = this.state;
    const { faqs = [], onChange } = this.props;
    if (editIndex === -1) {
      faqs.push({
        question,
        answer
      });
    } else {
      faqs[editIndex] = Object.assign({
        question,
        answer,
      });
    }
    onChange(e, {name: 'faqs', value: faqs});
    this.setState({
      editMode: false,
      editIndex: -1,
      question: '',
      answer: '',
    });
  }

  onDelete = (index) => {
    const { faqs = [], onChange } = this.props;
    faqs.splice(index, 1);
    onChange({}, {name: 'faqs', value: faqs});
  }

  onUpdate = (index) => {
    const { faqs = [] } = this.props;
    const { question, answer } = faqs[index];
    this.setState({
      question,
      answer,
      editIndex: index,
      editMode: true,
    })
  }

  render() {
    const { faqs = [] } = this.props;
    const { editMode, question, answer } = this.state;
    return (
      <Form.Field>
        <label>FAQs</label>
        {faqs.map((faq, index) => {
          const { question, answer } = faq;
          return (
            <div style={{display: 'flex', marginBottom: '10px'}} key={index}>
              <div style={{flex: 1}}>
                <div>
                  <strong>{question}</strong>
                  <div dangerouslySetInnerHTML={{__html: `${answer}`}}></div>
                </div>
              </div>
              <div>
                <Button icon onClick={this.onUpdate.bind(this, index)} type="button" style={{margin: '0 1em 1em 0'}}>
                  <Icon name="pencil" />
                </Button>
                <Button icon onClick={this.onDelete.bind(this, index)} type="button">
                  <Icon name="trash" />
                </Button>
              </div>
            </div>
          )
        })}
        <br />
        {editMode ?
          <div>
            <Input placeholder='Question' name="question" value={question} onChange={this.onChange} style={{marginBottom: '5px'}}/>
            <TextArea placeholder='Answer' name="answer" value={answer} onChange={this.onChange}/>
            <br /><br />
            <div>
              <Button type="button" positive disabled={!question || !answer} onClick={this.updateFAQ}>Add</Button>
              <Button type="button" negative onClick={() => {this.setState({editMode: false, question: '', answer: ''})}}>Cancel</Button>
            </div>
          </div> :
          <Button type="button" positive onClick={() => {this.setState({editMode: true, editIndex: -1})}}>Add</Button>
        }
      </Form.Field>
    )
  }
}