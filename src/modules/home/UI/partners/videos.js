import React, { Component } from 'react';
import { Button, Icon } from 'semantic-ui-react';
import { Form, Input } from 'semantic-ui-react';

export default class FAQs extends Component {
  state = {
    video: '',
    editIndex: -1,
  }

  onChange = (e, { name, value }) => {
    this.setState({
      [name]: value,
    })
  }

  addVideo = (e) => {
    const { editIndex, video } = this.state;
    const { videos = [], onChange } = this.props;
    if (editIndex === -1) {
      videos.push(video);
    } else {
      videos[editIndex] = video;
    }
    onChange(e, {name: 'videos', value: videos});
    this.setState({
      editIndex: -1,
      video: '',
    });
  }

  onDelete = (e, index) => {
    e.preventDefault();
    const { videos = [], onChange } = this.props;
    videos.splice(index, 1);
    onChange(e, {name: 'videos', value: videos});
    this.setState({
      editIndex: -1,
      video: '',
    });
  }

  onEdit = (index) => {
    const { videos } = this.props;
    const video = videos[index];
    this.setState({
      video,
      editIndex: index,
    })
  }

  render() {
    const { videos = [] } = this.props;
    const { video } = this.state;
    return (
      <Form.Field>
        <label>Videos</label>
        {videos.map((video, index) => {
          return (
            <Button.Group key={index} style={{margin: '5px', cursor: 'pointer'}}>
              <Button type="button" onClick={this.onEdit.bind(this, index)}>{video}</Button>
              <Button type="button" icon onClick={(e) => {this.onDelete(e, index)}}>
                <Icon name='close'/>
              </Button>
              <Button type="button" icon as={"a"} href={`https://www.youtube.com/watch?v=${video}`} target="_blank">
                <Icon name="external"/>
              </Button>
            </Button.Group>
          )
        })}
        <div style={{display: 'flex'}}>
          <Input placeholder='Youtube Video ID' name="video" value={video} onChange={this.onChange} style={{marginRight: '5px'}}/>
          <Button type="button" positive disabled={!video} onClick={this.addVideo}>Add Video</Button>
        </div>
      </Form.Field>
    )
  }
}