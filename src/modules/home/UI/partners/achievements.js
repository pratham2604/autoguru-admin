import React, { Component } from 'react';
import { Button, Icon } from 'semantic-ui-react';
import { Form, Input } from 'semantic-ui-react';

export default class Achievements extends Component {
  state = {
    editMode: false,
    newAchievement: '',
  }

  onChange = (e, { name, value }) => {
    if (name === 'newAchievement') {
      this.setState({
        newAchievement: value,
      });
      return;
    }
  }

  addAchievement = (e) => {
    const { achievements = [], onChange } = this.props;
    const { newAchievement } = this.state;
    achievements.push(newAchievement);
    onChange(e, {name: 'achievements', value: achievements});
    this.setState({
      editMode: false,
      newAchievement: '',
    });
  }

  onDelete = (index) => {
    const { achievements = [], onChange } = this.props;
    achievements.splice(index, 1);
    onChange({}, {name: 'achievements', value: achievements});
  }

  onUpdate = (index, value) => {
    const { achievements = [], onChange } = this.props;
    achievements[index] = value;
    onChange({}, {name: 'achievements', value: achievements});
  }

  render() {
    const { achievements = [] } = this.props;
    const { editMode, newAchievement } = this.state;
    return (
      <Form.Field>
        <label>Achievements</label>
        {achievements.map((achievement, index) => {
          return (
            <div style={{display: 'flex', marginBottom: '5px'}} key={index}>
              <Input placeholder='Achievement' value={achievement} onChange={(e) => {this.onUpdate(index, e.target.value)}} style={{marginRight: '1em'}}/>
              <Button icon onClick={this.onDelete.bind(this, index)} type="button">
                <Icon name="trash" />
              </Button>
            </div>
          )
        })}
        <br />
        {editMode ?
          <div>
            <Input placeholder='Achievement' name="newAchievement" value={newAchievement} onChange={this.onChange}/>
            <br /><br />
            <div>
              <Button type="button" positive disabled={!newAchievement} onClick={this.addAchievement}>Add</Button>
              <Button type="button" negative onClick={() => {this.setState({editMode: false, newAchievement: ''})}}>Cancel</Button>
            </div>
          </div> :
          <Button type="button" positive onClick={() => {this.setState({editMode: true})}}>Add</Button>
        }
      </Form.Field>
    )
  }
}