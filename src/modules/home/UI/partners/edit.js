import React, { Component } from 'react';
import { Segment, Button, Header } from 'semantic-ui-react';
import { Form, Input, TextArea } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import SocialLinks from './socialLinks';
import Achievements from './achievements';
import FAQs from './faqs';
import Services from './services';
import TimeLine from './timeline';
import Gallery from './gallery';
import Videos from './videos';
import ImageUploader from '../../../../components/imageUploader/imageUploader';

export default class extends Component {
  constructor(props) {
    super(props);
    this.state = {
      partner: {},
      isDataFetched: false,
    };
  }

  componentDidUpdate() {
    const { isDataFetched } = this.state;
    const { props = {}, partners } = this.props;
    const { params = {} } = props.match || {};
    const { id } = params;
    if (partners.length > 0 && id && id !== 'new-partner' && !isDataFetched) {
      const partner = partners.find(node => node.id === id);
      if (partner) {
        this.setState({
          partner,
          isDataFetched: true,
        })
      }
    }
  }

  onChange = (e, { name, value }) => {
    const { partner } = this.state;
    partner[name] = value;
    this.setState({
      partner,
    });
  }

  onChecked = (e, { name, value}) => {
    this.setState({
      [name]: !this.state[name],
    })
  }

  handleSubmit = () => {
    const { partner } = this.state;
    const data = Object.assign({}, partner);
    const method = data.id ? 'editData' : 'addData';
    console.log(data);
    this.props[method](data, 'partner');
    this.setState({
      isDataFetched: false,
    });
    const { props = {} } = this.props;
    const { history } = props;
    history.push('/partners')
  }

  render() {
    const { partner } = this.state;
    const {
      title = '',
      slug = '',
      url = '',
      logo = '',
      banner = '',
      shortDescription = '',
      description = '',
      achievementsTitle = '',
      links = {},
      achievements = [],
      faqs = [],
      services = [],
      timeline = [],
      images = [],
      videos = [],
    } = partner;
    const { routesAccess } = this.props;
    const currentRouteAccess = routesAccess.find(access => access.id === 'admin');
    const { edit } = currentRouteAccess || {};
    if (!edit) {
      return null;
    }

    return (
      <div>
        <Header as="h1">{title || 'New Partner Profile'}</Header>
        <Segment>
          <Form onSubmit={this.handleSubmit}>
            <Form.Field>
              <label>Title</label>
              <Input placeholder='Title' name="title" value={title} onChange={this.onChange}/>
            </Form.Field>
            <Form.Field>
              <label>Logo</label>
              <ImageUploader label="logo" name="logo" link={logo} onChange={this.onChange} />
            </Form.Field>
            <Form.Field>
              <label>Banner</label>
              <ImageUploader label="banner" name="banner" link={banner} onChange={this.onChange} />
            </Form.Field>
            <Form.Field>
              <label>Slug</label>
              <Input placeholder='Slug' name="slug" value={slug} onChange={this.onChange}/>
            </Form.Field>
            <Form.Field>
              <label>Url</label>
              <Input placeholder='Url' name="url" value={url} onChange={this.onChange}/>
            </Form.Field>
            <Form.Field>
              <label>Short Description</label>
              <Input placeholder='Short Description' name="shortDescription" value={shortDescription} onChange={this.onChange}/>
            </Form.Field>
            <Form.Field>
              <label>Description</label>
              <TextArea placeholder='Description' name="description" value={description} onChange={this.onChange}/>
            </Form.Field>
            <SocialLinks links={links} onChange={this.onChange} />
            <br/>
            <Form.Field>
              <label>Achievements Title</label>
              <Input placeholder='Achievements Title' name="achievementsTitle" value={achievementsTitle} onChange={this.onChange}/>
            </Form.Field>
            <Achievements achievements={achievements} onChange={this.onChange} />
            <br/>
            <FAQs faqs={faqs} onChange={this.onChange} />
            <br/>
            <Services services={services} onChange={this.onChange} />
            <br/>
            <TimeLine timeline={timeline} onChange={this.onChange} />
            <br/>
            <Gallery images={images}  onChange={this.onChange} />
            <br/>
            <Videos videos={videos} onChange={this.onChange} />
            <br/>
            {edit && <Button type='submit'>Submit</Button>}
            <Button type="button" color="red" as={Link} to="/partners">Cancel</Button>
          </Form>
        </Segment>
      </div>
    )
  }
}
