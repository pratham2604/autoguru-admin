import React, { Component } from 'react';
import { Segment, Button, Icon, Modal } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import { MDBDataTable } from 'mdbreact';

const columns = [{
  label: 'Title',
  field: 'title',
  sort: 'asc',
}, {
  label: 'Slug',
  field: 'slug',
  sort: 'asc',
}, {
  label: 'Actions',
  field: 'actions',
  sort: 'disabled',
}];

export default class extends Component {
  render() {
    const { partners, routesAccess, deleteData } = this.props;
    const currentRouteAccess = routesAccess.find(access => access.id === 'admin');
    const { edit } = currentRouteAccess || {};
    if (!edit) {
      return null;
    }

    const rows = partners.map(dataRow => {
      const { title, id, slug } = dataRow;

      return {
        title,
        slug,
        actions: (
          <div style={{display: 'flex'}}>
            <Button as={Link} to={`/partners/${id}`}>Edit Partner</Button>
            {edit && <DeleteModal data={dataRow} deleteData={deleteData} />}
          </div>
        )
      }
    });
    const tableData = {
      columns,
      rows
    };

    return (
      <Segment>
        {edit && <Button as={Link} to="/partners/new-partner">Add Partner</Button> }
        <MDBDataTable responsive striped bordered hover data={tableData} />
      </Segment>
    )
  }
}

class DeleteModal extends Component {
  state = {
    open: false,
  }

  onDelete = () => {
    const { data, deleteData } = this.props;
    deleteData(data, 'partner');
    this.setState({
      open: false,
    });
  }

  render() {
    const { open } = this.state;
    const { data } = this.props;
    const { title } = data;
    return (
      <Modal size="mini" open={open} trigger={
        <Button icon color="red" onClick={() => {this.setState({open: true})}}>
          <Icon name="delete" />
        </Button>
      } closeIcon style={{height: 'auto'}}>
        <Modal.Header>Delete {title}</Modal.Header>
        <Modal.Content>
          Are you sure want to delete ?
        </Modal.Content>
        <Modal.Actions>
            <Button
              negative
              icon="trash"
              labelPosition='right'
              content='Delete'
              onClick={this.onDelete}
            />
        </Modal.Actions>
      </Modal>
    )
  }
}