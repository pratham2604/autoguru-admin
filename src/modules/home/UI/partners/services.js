import React, { Component } from 'react';
import { Button, Icon } from 'semantic-ui-react';
import { Form, Input, TextArea } from 'semantic-ui-react';

export default class Services extends Component {
  state = {
    editMode: false,
    title: '',
    description: '',
    editIndex: -1,
  }

  onChange = (e, { name, value }) => {
    this.setState({
      [name]: value,
    })
  }

  updateService = (e) => {
    const { editIndex, title, description } = this.state;
    const { services = [], onChange } = this.props;
    if (editIndex === -1) {
      services.push({
        title,
        description
      });
    } else {
      services[editIndex] = Object.assign({
        title,
        description,
      });
    }
    onChange(e, {name: 'services', value: services});
    this.setState({
      editMode: false,
      editIndex: -1,
      title: '',
      description: '',
    });
  }

  onDelete = (index) => {
    const { services = [], onChange } = this.props;
    services.splice(index, 1);
    onChange({}, {name: 'services', value: services});
  }

  onUpdate = (index) => {
    const { services = [] } = this.props;
    const { title, description } = services[index];
    this.setState({
      title,
      description,
      editIndex: index,
      editMode: true,
    })
  }

  render() {
    const { services = [] } = this.props;
    const { editMode, title, description } = this.state;
    return (
      <Form.Field>
        <label>Services</label>
        {services.map((service, index) => {
          const { title, description } = service;
          return (
            <div style={{display: 'flex', marginBottom: '10px'}} key={index}>
              <div style={{flex: 1}}>
                <div>
                  <strong>{title}</strong>
                  <div>{description}</div>
                </div>
              </div>
              <div>
                <Button icon onClick={this.onUpdate.bind(this, index)} type="button" style={{margin: '0 1em 1em 0'}}>
                  <Icon name="pencil" />
                </Button>
                <Button icon onClick={this.onDelete.bind(this, index)} type="button">
                  <Icon name="trash" />
                </Button>
              </div>
            </div>
          )
        })}
        <br />
        {editMode ?
          <div>
            <Input placeholder='Title' name="title" value={title} onChange={this.onChange} style={{marginBottom: '5px'}}/>
            <TextArea placeholder='Description' name="description" value={description} onChange={this.onChange}/>
            <br /><br />
            <div>
              <Button type="button" positive disabled={!title || !description} onClick={this.updateService}>Add</Button>
              <Button type="button" negative onClick={() => {this.setState({editMode: false, title: '', description: ''})}}>Cancel</Button>
            </div>
          </div> :
          <Button type="button" positive onClick={() => {this.setState({editMode: true, editIndex: -1})}}>Add</Button>
        }
      </Form.Field>
    )
  }
}
