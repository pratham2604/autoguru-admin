import React, { Component } from 'react';
import { Form, Input } from 'semantic-ui-react';

export default class SocialLinks extends Component {
  onChange = (e, { name, value }) => {
    const { links = {}, onChange } = this.props;
    links[name] = value;
    onChange(e, {name: 'links', value: links});
  }

  render() {
    const { links = {} } = this.props;
    const { facebook = '', twitter = '', linkedIn = '', instagram = '' } = links;
    return (
      <Form.Field>
        <label>Links</label>
        <Input label="Facebook" placeholder='link' name="facebook" value={facebook} onChange={this.onChange}/>
        <br/><br/>
        <Input label="Twitter" placeholder='link' name="twitter" value={twitter} onChange={this.onChange}/>
        <br/><br/>
        <Input label="LinkedIn" placeholder='link' name="linkedIn" value={linkedIn} onChange={this.onChange}/>
        <br/><br/>
        <Input label="Instagram" placeholder='link' name="instagram" value={instagram} onChange={this.onChange}/>
      </Form.Field>
    )
  }
}