import React, { Component } from 'react';
import { Button, Form, Grid } from 'semantic-ui-react';
import ImageUploader from '../../../../components/imageUploader/imageUploader';

export default class Gallery extends Component {
  state = {
    editMode: false,
  }

  onChange = (data, index) => {
    const { images = [], onChange } = this.props;
    if (index === -1) {
      images.push(data.value);
    } else {
      if (data.value !== '') {
        images[index] = data.value;
      } else {
        images.splice(index, 1);
      }
    }
    onChange({}, {name: 'images', value: images})
    this.setState({
      editMode: false,
    });
  }

  render() {
    const { images = [] } = this.props;
    const { editMode } = this.state;
    return (
      <Form.Field>
        <label>Gallery</label>
        <Grid>
          <Grid.Row style={{padding: '1rem'}}>
            {images.map((image, index) => {
              return (
                <Grid.Column width={4} key={index} style={{marginBottom: '1rem'}}>
                  <ImageUploader computer={16} label="image" name="image" link={image} onChange={(e, data) => {this.onChange(data, index)}} />
                </Grid.Column>
              )
            })}
            <Grid.Column width={4} style={{marginTop: '1em'}}>
              {editMode ?
                <ImageUploader computer={16} label="image" name="image" link="" onChange={(e, data) => {this.onChange(data, -1)}} /> :
                <Button style={{marginTop: '1em'}} type="button" positive onClick={() => {this.setState({editMode: true})}}>Add image to Gallery</Button>
              }
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Form.Field>
    )
  }
}